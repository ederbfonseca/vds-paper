Uncertain tensors, including $n$-ary relations
and uncertain matrices, are often encountered.  For example, suppose
an analyst wants to discover groups of Twitter users who had been, for
some weeks, influential when they were writing about a subset of the
Brazilian soccer teams.  Using the number of retweets as proxy for the
influence, she decides to count how many times the messages written by
a given user and mentioning a given team were retweeted along a given
week.  After doing so for every $3$-tuple $($user$,$ team$,$ week$)$,
the analyst faces a $3$-way numerical tensor.  She can then turn every
(normalized) number of retweets in it into a value between $0$ and
$1$, where $0$ stands for ``absolutely not influential'' and $1$ for
``absolutely influential''.  For instance, she can choose the two
parameters of a logistic function (what is ``moderately influential''
and how sharp the transition between ``not influential'' and
``influential'') and use it.

\begin{figure*}
  \label{fig:example}
  \centering
  \footnotesize
  % \setlength{\tabcolsep}{2.5pt}
  \begin{tabular}{rccccccccccccc}
    & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}} & \quad
    \quad & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}}\\
    \texttt{ESPNagora} & \light 1.00 & \dark 0.85 & \light 0.80 &
    \light 0.70 & 0.10 & \dark 0.95 & & \light 0.70 & 0.20 & \darker
    1.00 & \darker 1.00 & 0.25 & \light 0.80\\
    \texttt{Estadao} & \light 0.80 & \dark 0.90 & \light 0.70 & \light
    0.80 & 0.20 & \dark 0.90 & & \light 0.80 & 0.05 & \darker 0.75 &
    \darker 0.60 & 0.00 & \light 0.70\\
    \texttt{FlaNews} & \light 0.70 & 0.15 & \light 1.00 & \light 0.65
    & 0.00 & \light 0.70 & & \light 0.60 & 0.00 & \darker 1.00 &
    \darker 1.00 & 0.00 & \light 0.65\\
    \texttt{N5Eoficial} & \light 0.05 & \dark 1.00 & \light 0.05 &
    \light 0.00 & 0.00 & \dark 0.80 & & \light 0.00 & 1.00 & \light
    0.55 & \light 0.50 & 0.05 & \light 0.40\\
    \texttt{RenataBFan} & \light 0.80 & 0.15 & \light 0.75 & \light
    0.75 & 0.95 & \light 0.90 & & \light 0.85 & 0.05 & \light 0.70 &
    \light 0.60 & 0.85 & \light 0.80\\
    \texttt{SporTV} & \light 0.80 & 0.55 & \light 0.70 & \light 0.60 &
    0.05 & \light 0.80 & & \light 0.80 & 0.20 & \darker 1.00 & \darker
    1.00 & 0.10 & \light 0.75\\
    & \multicolumn{6}{c}{\texttt{week 45}} & &
    \multicolumn{6}{c}{\texttt{week 46}}\\
    & & & & & & & & & & & & &
  \end{tabular}
  \caption{Three patterns with their densities (the $\lambda$ values
    on their sides), \ie, the average value in the sub-tensor they
    define, summing up the $2 \times 6 \times 6$ uncertain tensor
    above.  The color of a cell corresponds to the estimation of its
    value (a pattern density or $\lambda_0 = 0$ for white), according
    to the three-pattern disjunctive box cluster model.}
\end{figure*}
Collecting during 2 weeks the retweets that 6 users received writing
about 6 teams, the analyst may end up with the $3$-way uncertain
tensor in Fig.~\ref{fig:example}.  That same figure shows the summary
of the tensor, as output by an algorithm that agglomerates patterns and selects those agglomerates to fit a regression model. That summary is a \emph{disjunctive box cluster model}, a regression model proposed by Mirkin and
Kramarenko~\cite{mirkin:2011}.

The strategy of summarizing uncertain tensors with patterns using the \emph{disjunctive box cluster model} is very good. However, even so, the data set can still be represented by a high number of patterns that can extrapolate the human capacity for interpretation. In addition, patterns have attributes such as area, density, and intersections, and thus it becomes difficult to understand pattern properties in order to use information, observe proportions, and make analyzes and comparisons about their set.

Goethals \cite{goethals:2011} explains the results of one analysis often lead to new questions, requiring more analysis. In an ideal world, this process is streamlined. That is, data mining is not only iterative, but also interactive: the user can give such feedback immediately, and easily browse the results. In traditional pattern mining, however, algorithms typically produce large amounts of patterns, many of which are not interesting to the user, and the results are typically only given in a flat text file, making it hard to analyze the results. By instead providing an iterative and interactive process, the user would be able to explore and refine the discovered patterns on the fly.

The visualization techniques can either be used as data mining methods on their own, which is sometimes called “visual data mining” ~\cite{russell:2012}, or they can
collaborate with data mining algorithms to facilitate and speed up the analysis of data, intermediate results, or discovered knowledge.

In this paper, we present a framework in which we allow the user to interactively view mined patterns based on summary tensor. Our system visualizes all patterns discovered so far, yet, importantly lets the user interactively explore and dynamically modify these in an intuitive manner. By the visualization, users can browse through the mined data more easily and so quickly discover important information.