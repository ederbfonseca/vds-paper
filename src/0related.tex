\section{Related Work}


The above definition of a pattern is purely syntactical.  The data
mining literature proposes different semantics for such patterns in
0/1 or uncertain tensors.

\subsection{Complete Algorithms.}
\label{sec:complete_algorithms}

% \paragraph{0/1 Tensor Mining.}

Given a 0/1 tensor, several algorithms, notably
\textsc{Data-Peeler}~\cite{d-peeler}, list the closed patterns whose
$n$-tuples all have membership degrees equal to 1.  In
\cite{Ignatov2015}, Ignatov et al. survey relaxations of that
definition.  RMiner~\cite{rminer} does not relax
\textsc{Data-Peeler}'s definition but generalizes it to a
multi-relational context: 0/1 tensors sharing dimensions.  To tolerate
tuples with null membership degrees, A-RMiner~\cite{arminer} grows
intermediary patterns that RMiner computes and that still only
include tuples with membership degrees equal to 1.

% \paragraph{Uncertain Tensor Mining.}

\texttt{multidupehack}~\cite{cerf2014} generalizes
\textsc{Data-Peeler} so that uncertain tensors can be mined: $n$ upper
bounds $(\epsilon_1, \dots, \epsilon_n) \in \mathbb{R}_+^n$ specify
how much the total membership degree of the $n$-tuples in every
``slice'' of a pattern can deviate from the sum that would be obtained
if these membership degrees were all 1.  Formally, a pattern $X =
\prod_{i = 1}^n X_i$ output by \texttt{multidupehack} satisfies
$\forall i \in \{1, \dots, n\}$, $\forall e \in X_i$, $\sum_{t \in
  X\text{ s.t. }t_i =e} (1 - T_t) \leq \epsilon_i$.
\texttt{multidupehack}, like \textsc{Data-Peeler}, enforces as well a
closedness constraint, which discards all strict sub-patterns of a
valid pattern, and handles additional relevance constraints that prune
the search of the patterns satisfying them.  In particular, the
popular minimal size constraint --- ``involving at least $\gamma_i \in
\mathbb{N}$ elements of $D_i$'' --- greatly shortens the run time.
DCE~\cite{dce} is the only other complete algorithm to mine uncertain
tensors.  Using synthetic datasets, Cerf and Meira \cite{cerf2014}
show that DCE's definition of a valid pattern tends to catch patterns
that go over the edges of the patterns that were planted, whereas
\texttt{multidupehack}'s patterns do not, unless the tensor is very
noisy.  Moreover, \texttt{multidupehack} scales better than DCE.  Yet,
increasing its upper bounds $(\epsilon_1, \dots, \epsilon_n)$ still
exponentially influences the run time: given a reasonable time budget,
\texttt{multidupehack} can only return many overlapping fragments of a
large and noisy pattern to discover.

\subsection{Heuristic Algorithms.}
\label{sec:heuristic_algorithms}

% \paragraph{Agglomerating Patterns.}

\textsc{triCluster}~\cite{tricluster} can optionally post-process the
patterns it mines in $3$-way tensors.  It can merge two overlapping
patterns $X$ and $Y$ into $X \sqcup Y$.  That happens when $\frac{|X
  \cup Y|}{|X \sqcup Y| - |X \cup Y|}$ is large enough.  That process
ignores the membership degrees.  In contrast,
\textsc{Alpha}~\cite{alpha} takes into account the membership degrees
of the $n$-tuples in $X \sqcup Y$ to define the similarity between the
patterns $X$ and $Y$ in a 0/1 tensor $T$.  That similarity is $\min_{i
  = 1}^n \min_{e \in X_i \cup Y_i} \frac{\sum_{t \in X \sqcup Y\text{
      s.t. }t_i = e} T_t}{|\{t \in X \sqcup Y\text{ s.t. }t_i =
  e\}|}$, \ie, the smallest density among those of the ``slices'' of
$X \sqcup Y$.  \textsc{Alpha} hierarchically agglomerates patterns
according to that similarity.  It then selects a pattern in the
dendrogram if it is distant from its parent and close to only having
$n$-tuples with membership degrees equal to 1.  A difference combines
the two criteria.  \textsc{triCluster}'s and \textsc{Alpha}'s
similarities lack theoretical grounds.  Wong and Li~\cite{wong}
propose to hierarchically agglomerate patterns in 0/1 matrices.  The
distance between two patterns $X$ and $Y$ is a sum of normalized
entropy in the two rectangular regions of $(X \sqcup Y) \setminus (X
\cup Y)$, weighted by their respective areas.  That computation sees
the elements in one of the two dimensions as independent probabilistic
events and the distance between two patterns depends on the choice of
this dimension, \ie, the distance between $X$ and $Y$ is different if
the 0/1 matrix is transposed.  It therefore is unclear how the
distance could be generalized to patterns in 0/1 tensors.

% \paragraph{Directly Mining 0/1 Tensors.}

Several works %, \eg, \cite{Miettinen11,ErdosM13,park2017fast}
approximately factorize $n$-way 0/1 tensors.  To be easily interpreted
as $r$ patterns, the $n$ factors of the rank-$r$ CANDECOMP/PARAFAC
(CP) decomposition are forced to be 0/1 too.  More precisely, the
Boolean rank-$r$ CP decomposition of a tensor $T \in \{0, 1\}^{D_1
  \times \dots \times D_n}$ aims to discover $n$ matrices $A^1 \in
\{0, 1\}^{|D_1| \times r}$, \dots, $A^n \in \{0, 1\}^{|D_n| \times r}$
that minimize $\|T - \max_{k = 1}^r A_{:k}^1 \otimes \dots \otimes
A_{:k}^n\|$, where $A_{:k}^i$ denotes the $k^{\text{th}}$ column of
$A^i$, $\otimes$ is the outer product, $\|.\|$ is the Frobenius norm,
and $\max_{k = 1}^r$ returns a tensor where the membership degree of
an $n$-tuple is the maximal membership degree of this $n$-tuple in the
$r$ tensors.  The $r$ patterns are the arguments of $\max_{k = 1}^r$.
The \texttt{BCP\_ALS} algorithm~\cite{Miettinen11} finds them by
Alternating Least Squares.  Park et al.~\cite{park2017fast} present a
distribution of that method on the Spark framework.  Their
implementation, named DBTF, exhibits near-linear scalability
w.r.t. the size of the 0/1 tensor, its density, the rank $r$ and the
number of machines.  The \textsc{Walk'n'Merge}
algorithm~\cite{ErdosM13} discovers patterns through successive random
walks in a graph: its vertices stand for the $n$-tuples with
membership degrees at $1$ and its edges link $n$-tuples that differ in
one single dimension.  The dense-enough patterns are then completed
with \textsc{Data-Peeler}-like patterns.  Pairs of patterns are
agglomerated if the result is sufficiently dense.  The patterns that
most decrease the objective function are greedily output.  That
process stops when the model would overfit the data according to the
Minimal Description Length principle.

% Several works, \eg, \cite{tensor_factorization}, propose to
% approximately factorize $n$-way 0/1 tensors.  The $n$ factors, Boolean
% as well, define $k$ patterns, where $k \in \mathbb{N}$ is chosen by
% the analyst.  The factors are computed so that their product (with $1
% + 1 = 1$) is as similar as possible to the processed Boolean tensor.
% The patterns minimizing the norm of the difference are large and
% dense.

Mirkin and Kramarenko~\cite{mirkin2011} map pattern mining in 0/1
tensors to a regression problem: explaining the membership degrees in
the tensor $T$ with a set $\mathcal{X}$ of patterns.  A parameter
$\lambda_X \in \mathbb{R}$ is estimated for every pattern $X \in
\mathcal{X}$ and the regression model, called \emph{disjunctive box
  cluster model}, predicts that $T_t$ is
\begin{align}
  \label{eq:model}
  \hat{T}_t = \begin{cases}
    \displaystyle\max_{X \in \mathcal{X}\text{ s.t. }t \in X}
    \lambda_X + \lambda_0 & \text{if }t \in \cup_{X \in \mathcal{X}}
    X\\
    ~~~~~~\lambda_0 & \text{otherwise}
  \end{cases}
\end{align}
where the intercept $\lambda_0 \in [0, 1]$ is fixed by the analyst.
Ordinary least squares guide the selection of the model, \ie, the set
$\mathcal{X}$ of patterns aims to minimize
\begin{equation}
  \label{eq:rss}
  RSS_T(\mathcal{X}) = \sum_{t \in \sizea} (T_t - \hat{T}_t)^2.
\end{equation}
This is $\|T - \lambda_0\mathds{1} - \max_{k = 1}^r \lambda_k A_{:k}^1
\otimes \dots \otimes A_{:k}^n\|^2$, using the formalism of the
Boolean CP decomposition.  If $\lambda_0 = 0$, that decomposition
therefore aims to minimize $RSS_T$ fixing $\forall k \in \{1, \dots,
r\}$, $\lambda_k = 1$.

In~\cite{mirkin2011}, those are parameters.  The proposed
\emph{TriclusterBox} algorithm repeatedly searches for one single
pattern $X$ and the related parameter $\lambda_X$ that locally
minimize $X \mapsto RSS_T(\{X\})$.  Clearly, the optimal $\lambda_X$
is $\frac{\sum_{t \in X} (T_t - \lambda_0)}{|X|}$ (after a similarity
shift by $\lambda_0$, the density of $X$), \ie, only $X$ is to be
searched.  \emph{TriclusterBox} repeatedly takes one pattern that
involves one single element in each of the $n - 1$ first dimensions
and, in the last dimension, all elements such that the pattern only
includes $n$-tuples with membership degrees equal to 1.  Each of those
$\prod_{i = 1}^{n - 1} |D_i|$ patterns is the starting point of an
hill-climbing optimization.  Every iteration leads to a pattern that
involves one more or one less element.  The element is chosen so that
$X \mapsto RSS_T(\{X\})$ decreases as much as possible.  Hill-climbing
stops once any choice would increase $X \mapsto RSS_T(\{X\})$.
