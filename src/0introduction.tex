%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.

%% the only exception to this rule is the \firstsection command
\firstsection{Introduction}

\maketitle

%% \section{Introduction} %for journal use above \firstsection{..} instead
Uncertain tensors, including $n$-ary relations
and uncertain matrices, are often encountered.  For example, suppose
an analyst wants to discover groups of Twitter users who had been, for
some weeks, influential when they were writing about a subset of the
Brazilian soccer teams.  Using the number of retweets as proxy for the
influence, she decides to count how many times the messages written by
a given user and mentioning a given team were retweeted along a given
week.  After doing so for every $3$-tuple $($user$,$ team$,$ week$)$,
the analyst faces a $3$-way numerical tensor.  She can then turn every
(normalized) number of retweets in it into a value between $0$ and
$1$, where $0$ stands for ``absolutely not influential'' and $1$ for
``absolutely influential''.  For instance, she can choose the two
parameters of a logistic function (what is ``moderately influential''
and how sharp the transition between ``not influential'' and
``influential'') and use it.

\begin{figure*}
  \label{fig:example}
  \centering
  \footnotesize
  % \setlength{\tabcolsep}{2.5pt}
  \begin{tabular}{rccccccccccccc}
    & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}} & \quad
    \quad & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}}\\
    \texttt{ESPNagora} & \light 1.00 & \dark 0.85 & \light 0.80 &
    \light 0.70 & 0.10 & \dark 0.95 & & \light 0.70 & 0.20 & \darker
    1.00 & \darker 1.00 & 0.25 & \light 0.80\\
    \texttt{Estadao} & \light 0.80 & \dark 0.90 & \light 0.70 & \light
    0.80 & 0.20 & \dark 0.90 & & \light 0.80 & 0.05 & \darker 0.75 &
    \darker 0.60 & 0.00 & \light 0.70\\
    \texttt{FlaNews} & \light 0.70 & 0.15 & \light 1.00 & \light 0.65
    & 0.00 & \light 0.70 & & \light 0.60 & 0.00 & \darker 1.00 &
    \darker 1.00 & 0.00 & \light 0.65\\
    \texttt{N5Eoficial} & \light 0.05 & \dark 1.00 & \light 0.05 &
    \light 0.00 & 0.00 & \dark 0.80 & & \light 0.00 & 1.00 & \light
    0.55 & \light 0.50 & 0.05 & \light 0.40\\
    \texttt{RenataBFan} & \light 0.80 & 0.15 & \light 0.75 & \light
    0.75 & 0.95 & \light 0.90 & & \light 0.85 & 0.05 & \light 0.70 &
    \light 0.60 & 0.85 & \light 0.80\\
    \texttt{SporTV} & \light 0.80 & 0.55 & \light 0.70 & \light 0.60 &
    0.05 & \light 0.80 & & \light 0.80 & 0.20 & \darker 1.00 & \darker
    1.00 & 0.10 & \light 0.75\\
    & \multicolumn{6}{c}{\texttt{week 45}} & &
    \multicolumn{6}{c}{\texttt{week 46}}\\
    & & & & & & & & & & & & &
  \end{tabular}
  \includegraphics{pictures/patterns}
  \caption{Three patterns with their densities (the $\lambda$ values
    on their sides), \ie, the average value in the sub-tensor they
    define, summing up the $2 \times 6 \times 6$ uncertain tensor
    above.  The color of a cell corresponds to the estimation of its
    value (a pattern density or $\lambda_0 = 0$ for white), according
    to the three-pattern disjunctive box cluster model.}
\end{figure*}

Collecting during 2 weeks the retweets that 6 users received writing
about 6 teams, the analyst may end up with the $3$-way uncertain
tensor in Fig.~\ref{fig:example}.  That same figure shows the summary
of the tensor, as output by an algorithm that agglomerates patterns and selects those agglomerates to fit a regression model. That summary is a \emph{disjunctive
  box cluster model}, a regression model proposed by Mirkin and
Kramarenko~\cite{Mirkin:2011}.  In that model, patterns (\ie,
sub-tensors), which can be nested (as in Fig.~\ref{fig:example}), are
explanatory variables.  The algorithm constructs such patterns by growing and
hierarchically agglomerating the fragments that a complete algorithm
provides.  A subset of patterns is then selected to get a model that
fits (but does not overfit) the uncertain tensor, \ie, that accurately
predicts every value in it.  That prediction is the density of the
densest selected pattern that includes the related $n$-tuple; a
constant $\lambda_0 \in [0, 1]$ if no selected pattern includes it.

Given $n$ \emph{dimensions} (\ie, $n$ finite sets, assumed disjoint
w.l.o.g.) $D_1$, \dots, $D_n$, an \emph{uncertain tensor} $T$ maps any
$n$-tuple $t \in \sizea$ (where $\prod$ denotes the Cartesian product)
to a value $T_t \in [0, 1]$, called \emph{membership degree} of $t$.
Given an $n$-tuple $t$ and $i \in \{1, \dots, n\}$, $t_i$ denotes the
$i^{th}$ component of $t$, hence an element of $D_i$.

A pattern in $T$ is the Cartesian product of subsets of
each of the $n$ dimensions.  Formally, a set $X$ of $n$-tuples is a
\emph{pattern} if and only if there exists $X_1 \subseteq D_1$, \dots,
$X_n \subseteq D_n$ such that $X = \prod_{i = 1}^n X_i$.
Agglomerating $X = \prod_{i = 1}^n X_i$ and $Y = \prod_{i = 1}^n Y_i$
results in the smallest pattern including both $X$ and $Y$, \ie,
$\prod_{i = 1}^n X_i \cup Y_i$.  It is denoted $X \sqcup Y$.

Mirkin and Kramarenko \cite{Mirkin:2011} map pattern mining in 0/1
tensors to a regression problem: explaining the membership degrees in
the tensor $T$ with a set $\mathcal{X}$ of patterns.  A parameter
$\lambda_X \in \mathbb{R}$ is estimated for every pattern $X \in
\mathcal{X}$ and the regression model, called \emph{disjunctive box
  cluster model}, predicts that $T_t$ is
\begin{align}
  \label{eq:model}
  \hat{T}_t = \begin{cases}
    \displaystyle\max_{X \in \mathcal{X}\text{ s.t. }t \in X}
    \lambda_X + \lambda_0 & \text{if }t \in \cup_{X \in \mathcal{X}}
    X\\
    ~~~~~~\lambda_0 & \text{otherwise}
  \end{cases}
\end{align}
where the intercept $\lambda_0 \in [0, 1]$ is fixed by the analyst.
Ordinary least squares guide the selection of the model, \ie, the set
$\mathcal{X}$ of patterns aims to minimize
\begin{equation}
  \label{eq:rss}
  RSS_T(\mathcal{X}) = \sum_{t \in \sizea} (T_t - \hat{T}_t)^2.
\end{equation}

In a model composed of a single pattern $X$, \cite{Mirkin:2011} proposed that related parameter $\lambda_X$ must be $\frac{\sum_{t \in X} (T_t - \lambda_0)}{|X|}$, \ie, the pattern density (after a similarity shift) that minimize $RSS_T(\{X\})$. They proposed too that $RSS_T(\emptyset) - RSS_T(\{X\}) = |X|\lambda_x^2$, \ie, the pattern area times the density squared, which can be seen as an individual contribution of $X$ to the description of $T$.

As a whole, a \emph{disjunctive box cluster model} can be ordered so the \emph{$k^th$} pattern $X^k$ that minimize $RSS_T(\{X^1, \dots, X^{k-1}\}) - RSS_T(\{X^1, \dots, X^k\})$. Thus, the analyst who does not want to interpret all the patterns can, through the analysis of the first \emph{k} patterns, have a good summary of the description of $T$.

The strategy of summarizing uncertain tensors with patterns using the \emph{disjunctive box cluster model} is very good. However, even so, the data set can still be represented by a high number of patterns that can extrapolate the human capacity for interpretation. In addition, patterns have attributes such as area, density, and intersections, and thus it becomes difficult to understand pattern properties in order to use information, observe proportions, and make analyzes and comparisons about their set.

Goethals \cite{Goethals:2011} explains the results of one analysis often lead to new questions, requiring more analysis. In an ideal world, this process is streamlined. That is, data mining is not only iterative, but also interactive: the user can give such feedback immediately, and easily browse the results. In traditional pattern mining, however, algorithms typically produce large amounts of patterns, many of which are not interesting to the user, and the results are typically only given in a flat text file, making it hard to analyze the results. By instead providing an iterative and interactive process, the user would be able to explore and refine the discovered patterns on the fly.

In this paper, we present a framework in which we allow the user to interactively view mined patterns based on summary tensor. Our system visualizes all patterns discovered so far, yet, importantly lets the user interactively explore and dynamically modify these in an intuitive manner. By the visualization, users can browse through the mined data more easily and so quickly discover important information.



 
