In a regression model proposed by Mirkin and Kramarenko~\cite{mirkin:2011}, patterns (\ie, sub-tensors), which can be nested (as in Fig.~\ref{fig:example}), are explanatory variables.  The algorithm constructs such patterns by growing and hierarchically agglomerating the fragments that a complete algorithm provides.  A subset of patterns is then selected to get a model that fits (but does not overfit) the uncertain tensor, \ie, that accurately predicts every value in it.  That prediction is the density of the
densest selected pattern that includes the related $n$-tuple; a
constant $\lambda_0 \in [0, 1]$ if no selected pattern includes it.

Given $n$ \emph{dimensions} (\ie, $n$ finite sets, assumed disjoint
w.l.o.g.) $D_1$, \dots, $D_n$, an \emph{uncertain tensor} $T$ maps any
$n$-tuple $t \in \sizea$ (where $\prod$ denotes the Cartesian product)
to a value $T_t \in [0, 1]$, called \emph{membership degree} of $t$.
Given an $n$-tuple $t$ and $i \in \{1, \dots, n\}$, $t_i$ denotes the
$i^{th}$ component of $t$, hence an element of $D_i$.

A pattern in $T$ is the Cartesian product of subsets of
each of the $n$ dimensions.  Formally, a set $X$ of $n$-tuples is a
\emph{pattern} if and only if there exists $X_1 \subseteq D_1$, \dots,
$X_n \subseteq D_n$ such that $X = \prod_{i = 1}^n X_i$.
Agglomerating $X = \prod_{i = 1}^n X_i$ and $Y = \prod_{i = 1}^n Y_i$
results in the smallest pattern including both $X$ and $Y$, \ie,
$\prod_{i = 1}^n X_i \cup Y_i$.  It is denoted $X \sqcup Y$.

Mirkin and Kramarenko \cite{mirkin:2011} map pattern mining in 0/1
tensors to a regression problem: explaining the membership degrees in
the tensor $T$ with a set $\mathcal{X}$ of patterns.  A parameter
$\lambda_X \in \mathbb{R}$ is estimated for every pattern $X \in
\mathcal{X}$ and the regression model, called \emph{disjunctive box
  cluster model}, predicts that $T_t$ is
\begin{align}
  \label{eq:model}
  \hat{T}_t = \begin{cases}
    \displaystyle\max_{X \in \mathcal{X}\text{ s.t. }t \in X}
    \lambda_X + \lambda_0 & \text{if }t \in \cup_{X \in \mathcal{X}}
    X\\
    ~~~~~~\lambda_0 & \text{otherwise}
  \end{cases}
\end{align}
where the intercept $\lambda_0 \in [0, 1]$ is fixed by the analyst.
Ordinary least squares guide the selection of the model, \ie, the set
$\mathcal{X}$ of patterns aims to minimize
\begin{equation}
  \label{eq:rss}
  RSS_T(\mathcal{X}) = \sum_{t \in \sizea} (T_t - \hat{T}_t)^2.
\end{equation}

In a model composed of a single pattern $X$, \cite{Mirkin:2011} proposed that related parameter $\lambda_X$ must be $\frac{\sum_{t \in X} (T_t - \lambda_0)}{|X|}$, \ie, the pattern density (after a similarity shift) that minimize $RSS_T(\{X\})$. They proposed too that $RSS_T(\emptyset) - RSS_T(\{X\}) = |X|\lambda_x^2$, \ie, the pattern area times the density squared, which can be seen as an individual contribution of $X$ to the description of $T$.

As a whole, a \emph{disjunctive box cluster model} can be ordered so the \emph{$k^th$} pattern $X^k$ that minimize $RSS_T(\{X^1, \dots, X^{k-1}\}) - RSS_T(\{X^1, \dots, X^k\})$. Thus, the analyst who does not want to interpret all the patterns can, through the analysis of the first \emph{k} patterns, have a good summary of the description of $T$.
