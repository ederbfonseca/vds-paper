\section{Relevant Information Derived from the Disjunctive Box Cluster
  Model}
\label{sec:model}

\subsection{Disjunctive Box Cluster Model}

Given $n \in \mathbb{N}$ \emph{dimensions} (\ie, $n$ finite sets,
assumed disjoint without loss of generality) $D_1$, \dots, $D_n$, a
\emph{fuzzy tensor} $T$ maps any $n$-tuple $t \in \sizea$ (where
$\prod$ denotes the Cartesian product) to a value $T_t \in [0, 1]$,
called \emph{membership degree} of $t$.  Figure~\ref{fig:tensor} shows
an example of a tiny fuzzy tensor.  Its $n = 3$ dimensions are
$\{$\texttt{week 45}$,$ \texttt{week 46}$\}$,
$\{$\texttt{ESPNagora}$,$ \texttt{Estadao}$,$ \texttt{FlaNews}$,$
\texttt{N5Eoficial}$,$ \texttt{RenataBFan}$,$ \texttt{SporTV}\}, and
$\{$\texttt{Corinthians}$,$ \texttt{Cruzeiro}$,$ \texttt{Flamengo}$,$
\texttt{Fluminense}$,$ \texttt{Internacional}$,$ \texttt{São
  Paulo}$\}$.  A \emph{pattern} is a set of $n$-tuples covered by a
sub-tensor.  More precisely, $X \subseteq \sizea$ is a \emph{pattern}
if and only if $\forall i \in \{1, \dots, n\}$,
$\exists X_i \subseteq D_i \mid X = \prod_{i = 1}^n X_i$.  The number
of $n$-tuples in a pattern $X$ is called its \emph{area} and is
denoted $|X|$.  In the table at the bottom of Figure~\ref{fig:tensor},
every row specifies a pattern: the Cartesian product of the sets in
the three columns.  The areas of those patterns are
$2 \times 6 \times 4 = 48$, $1 \times 3 \times 2 = 6$, and
$1 \times 4 \times 2 = 8$.

% Given two patterns $X$ and $Y$, $X$ is a \emph{sub-pattern} of $Y$ and
% $Y$ is a \emph{super-pattern} of $X$ if and only if $X \subseteq Y$.
% An agglomeration operator, $\sqcup$, will be needed further in this
% section: agglomerating the two patterns $X = \prod_{i = 1}^n X_i$ and
% $Y = \prod_{i = 1}^n Y_i$ results in the smallest super-pattern of
% both $X$ and $Y$, \ie, $X \sqcup Y = \prod_{i = 1}^n X_i \cup Y_i$.

The \emph{disjunctive box cluster model}, as first defined by Mirkin
and Kramarenko~\cite{Mirkin:2011}, is a regression model.  Its
explanatory variables are patterns and the target variable is the
fuzzy tensor $T$ they are in.  Given a set of patterns $\mathcal{X}$
(the explanatory variables), the predicted membership degree of an
$n$-tuple $t \in \sizea$ (the associated value in the pattern-based
reconstruction of the tensor) is:
\begin{align}
  \hat{T}_t = \begin{cases}
    \lambda_0 + \displaystyle\max_{X \in \mathcal{X}\text{ s.t. }t \in
      X} \lambda_X & \text{if }t \in \displaystyle\cup_{X \in
      \mathcal{X}} X\\
    \lambda_0 & \text{otherwise}
  \end{cases}
  \label{eq:model}
\end{align}
In this model, $\lambda_0$ is a constant, which is discussed in the
next paragraph, and $\lambda_0 + \lambda_X$ is the density of the
pattern $X$ in the tensor $T$, \ie,
$\lambda_0 + \lambda_X = \frac{\sum_{t \in X} T_t}{|X|}$.  Predicting
that membership degree for an $n$-tuple inside the pattern $X$ is
justified by the ordinary least-squares approach.  Indeed, assuming
that the patterns in $\mathcal{X}$ are disjoint, setting every
$\lambda_X$ so that $\lambda_0 + \lambda_X$ is the density of the
related pattern minimizes the residual sum of squares $RSS_T$ of the
regression:
\begin{align}
  RSS_T(\mathcal{X}) = \sum_{t \in \sizea} (T_t - \hat{T}_t)^2
  \label{eq:rss}
\end{align}

According to (\ref{eq:model}), $\lambda_0$ is the predicted membership
degree of any $n$-tuple that no pattern includes.  The density of the
whole tensor $T$, $\frac{\sum_{t \in \sizea} T_t}{|\sizea|}$, can be
considered its default value because that choice minimizes
$RSS_T(\emptyset)$.  Large fuzzy tensors being usually sparse, that
default value is typically close to $0$.  Nevertheless, the analyst
may fix $\lambda_0$ a different value to bias the search of the
relevant patterns, a topic outside the scope of this article.  A
larger $\lambda_0$ biases the search toward the discovery of more
patterns that are smaller and denser.  Figure~\ref{fig:reconstruction}
shows the tensor $\hat{T}$ that the three patterns in
Figure~\ref{fig:tensor} predict.  In that toy example,
$\lambda_0 = 0$.

Because $\lambda_0$ is predicted outside all patterns and
$\lambda_0 + \lambda_X$ is predicted for the $n$-tuples inside a
pattern $X$, unless a denser pattern overlaps, it makes sense to
visualize those quantities.  $\lambda_0 + \lambda_X$ being the density
of $X$, this quantity is easy to interpret.

\subsection{Contribution of a Single Pattern to the Model}

As already noticed in \cite{Mirkin:2011} (whereas, starting with next
paragraph, the rest of this article is entirely novel),
$RSS_T(\emptyset) - RSS_T(\{X\}) = |X|\lambda_X^2$ is a measure of the
contribution of a pattern $X$ to explaining the fuzzy tensor $T$.
Indeed, it is the difference between the residual sums of squares of
the model with no pattern ($\forall t \in \sizea$,
$\hat{T}_t = \lambda_0$) and of the model that only uses $X$ as an
explanatory variable.  Since the contribution of a pattern linearly
grows with its area, $|X|$, it makes sense to visualize this quantity.

\subsection{Coarser Models}
\label{sec:coarser}

Because $|X|\lambda_X^2$ assumes that the pattern $X$ is the
\emph{only} explanatory variable, it is a measure of the
\emph{individual} contribution of $X$ to explaining the fuzzy tensor
$T$.  For that reason, the measure does help much to select subsets of
$\mathcal{X}$ that relate to good coarser models of $T$.  Such models
are of interest if interpreting all the patterns in $\mathcal{X}$ is
considered too time-consuming.  If one \emph{single} pattern
$X \in \mathcal{X}$ must remain, it must indeed be the one maximizing
$|X|\lambda_X^2$.  However, if a second pattern $Y$ must complement
the partial explanation of the fuzzy tensor that $X$ provides, then
the pattern $Y$ that gives the second highest individual contribution
to the model is not necessarily the best choice.  The reason deals
with information redundancy if $X$ and $Y$ overlap: from the use of
$X$ as the sole explanatory variable to that of both $X$ and $Y$, the
predicted membership degree of an $n$-tuple in $X \cap Y$ cannot
improve as much as it would from the model using no pattern to the one
using only $Y$, \ie, not as much as the formula
$RSS_T(\emptyset) - RSS_T(\{Y\}) = |Y|\lambda_{Y}^2$ assumes.

The best second pattern, to complement the first pattern $X$, is the
pattern $Y$ that maximizes $RSS_T(\{X\}) - RSS_T(\{X, Y\})$: its
inclusion in the model reduces the residual sum of squares as much as
possible.  More generally, given a subset of patterns
$\mathcal{X}' \subset \mathcal{X}$, the pattern
$Y \in \mathcal{X} \setminus \mathcal{X}'$ that best complements
$\mathcal{X}'$ is the one that maximizes
$RSS_T(\mathcal{X}') - RSS_T(\mathcal{X}' \cup \{Y\})$.  Starting with
the empty set, that greedy selection is applied $|\mathcal{X}|$ times.
Known in the literature on regression under the name \emph{forward
  selection}, that simple algorithm allows here to sort $\mathcal{X}$
in $O((\sum_{Y \in \mathcal{X}} |Y|)^2)$ time.  Indeed, computing
$RSS_T(\mathcal{X}') - RSS_T(\mathcal{X}' \cup \{Y\})$ only requires
accessing the values $T_t$ and $\hat{T}_t$ for $t \in Y$ (selecting
$Y$ does not alter $\hat{T}_t$ for $t \notin Y$).  That rather low
computational cost is paid once.  Whatever the number
$k \leq |\mathcal{X}|$ of patterns the analyst chooses to visualize,
she is simply shown the $k$ first patterns in the sorted set.  A
non-greedy algorithm could allow to select another subset of $k$
patterns that better fits the fuzzy tensor, \ie, associated with a
smaller residual sum of squares.  Nevertheless, besides an higher
computational cost, it would be confusing, in the context of an
interactive visualization: an increase of the number $k$ of patterns
would discard patterns that were previously shown.

\subsection{Inclusions between Patterns}

In disjunctive box cluster models, patterns can be nested.  For
example, in Figure~\ref{fig:tensor}, the first pattern includes the
third one.  The third pattern supports a finer analysis of the
$3$-tuples in the first pattern: the first pattern is not homogeneous,
it includes a denser pattern, the third pattern.  More generally, the
patterns that are not included in any other pattern of $\mathcal{X}$
provide a high-level summary of the fuzzy tensor.  If one of those
patterns, $X$, includes other patterns
$\mathcal{X}' \subset \mathcal{X}$, then these other patterns support
a finer analysis of the $n$-tuples in $X$, whose membership degrees
are not uniformly distributed.  The disjunctive box cluster model
restricted to the explanatory variables in $\mathcal{X}'$ is
fundamentally a nested disjunctive box cluster model, a model of the
sub-tensor that $X$ defines.  It makes sense to exploit that
hierarchical aspect in the visualization \ie, to first display the
highest-level summary of the whole fuzzy tensor (patterns not included
in any other patterns of $\mathcal{X}$), to indicate the existence of
nested patterns in some of these patterns, and to allow the
visualization of the related nested disjunctive box cluster models,
whose patterns may, themselves, contain patterns, etc.

In the worst case, testing whether the pattern
$X = \prod_{i = 1}^n X_i$ includes the pattern
$Y = \prod_{i = 1}^n Y_i$ takes $O(\sum_{i = 1}^n |X_i| + |Y_i|)$
time, after $X_1$, \dots, $X_n$, $Y_1$, \dots, $Y_n$ have been sorted
(once for all inclusions involving these patterns).  As a consequence,
listing all pairwise inclusions in $\mathcal{X}$ requires up to
$O(\sum_{(X, Y) \in \mathcal{X}^2} \sum_{i = 1}^n |X_i| + |Y_i|)$
time.  Considering those inclusions as edges and the patterns as
vertices, a directed acyclic graph (DAG) is obtained.  Its sources are
the patterns in the highest-level summary of the fuzzy tensor.  In
$O(|\mathcal{X}|^3)$ time, a transitive reduction removes the edge
from any pattern $X \in \mathcal{X}$ to any other pattern
$Z \in \mathcal{X}$ if there exists a third pattern
$Y \in \mathcal{X}$ such that $X \supset Y \supset Z$.  After that
reduction, executed once, the successors of a vertex are the patterns
in the related nested disjunctive box cluster model, unless a
constraint is enforced.

A constraint $\mathcal{C}$ is a function that takes as input a pattern
of $\mathcal{X}$ and returns either ``true'', which means ``valid'',
or ``false'', which means ``invalid''.  Section~\ref{sec:coarser}
gives an example of a constraint: ``being in the coarse model with $k$
patterns'', which can be formalized as $\mathcal{C}(X) \equiv$
pos$(X) \leq k$, where pos$(X)$ is the position of $X$ in
$\mathcal{X}$, once $\mathcal{X}$ greedily sorted as explained in that
section.  Another example is $\mathcal{C}(X) \equiv$
pos$(X) \leq k \wedge \mathtt{Flamengo} \in X_3$, which complements
the previous constraint by additionally forcing the pattern to involve
$\mathtt{Flamengo}$, an element of the third dimension of the fuzzy
tensor.  Some of the sources of the DAG (for the highest-level summary
of the fuzzy tensor) or some successors of a vertex (for the related
nested disjunctive box cluster model) may be invalid.  If so, they
must be substituted by their successors, or, for those that are
invalid as well, their successors, etc.  From the set of patterns $V$
that would be visualized in absence of constraint (either the sources
of the DAG or the successors of a vertex), Algorithm~\ref{alg:ch}
returns the patterns to actually visualize given the constraint
$\mathcal{C}$.

\begin{algorithm}
  \caption{\texttt{filter-and-replace}}
  \label{alg:ch}
  \KwIn{set of patterns $V$, constraint $\mathcal{C}$, successor
    function $out$}
  \DontPrintSemicolon
  $\mathcal{S} \leftarrow \emptyset$\;
  \For{$v \in V$}{
    \eIf{$\mathcal{C}(v)$}{
      $\mathcal{S} \leftarrow \mathcal{S} \cup \{v\}$
    }{
      $\mathcal{S} \leftarrow \mathcal{S} \cup
      \texttt{filter-and-replace}(out(v), \mathcal{C}, out)$
    }
  }
  \Return $\mathcal{S}$
\end{algorithm}

\subsection{Partial Intersections between Patterns}

Patterns can \emph{partially} intersect too.  For example, in
Figure~\ref{fig:tensor}, the first pattern, of area $48$, and the
second pattern, of area $6$, have three $3$-tuples in common:
$\{$\texttt{ESPNagora}$,$ \texttt{Estadao}$,$
\texttt{N5Eoficial}$\} \times
\{$\texttt{Cruzeiro}$\}
\times \{$\texttt{week
  45}$\}$.  Alone in a disjunctive box cluster model, a pattern $X$
predicts that the membership degree of any of its $n$-tuples is the
density of $X$.  Nevertheless, the whole model may actually make
different predictions, not only because patterns can be nested (as
explained in the previous paragraph), but also because, at the partial
intersections of $X$ with denser patterns, the highest density is
predicted, according to (\ref{eq:model}).  The sizes of those pairwise
intersections are relevant pieces of information: patterns that
significantly overlap with the currently studied pattern $X$ may
significantly alter the conclusions that would be drawn from $X$
alone.  Visualizing the sizes of the intersections helps the
navigation to those other patterns and reduces the risk of a
misinterpretation of the disjunctive box cluster model.  Computing the
size of the intersection between two patterns
$X = \prod_{i = 1}^n X_i$ and $Y = \prod_{i = 1}^n Y_i$, \ie,
$|X \cap Y|$, only requires $O(\sum_{i = 1}^n |X_i| + |Y_i|)$ time,
after $X_1$, \dots, $X_n$, $Y_1$, \dots, $Y_n$ have been sorted (once
for all intersections involving these patterns).  That low cost
enables the computation of many sizes of intersection in a sensibly
immediate response to an event the analyst triggers in the interactive
visualization.

\subsection{Similarities between Patterns}

The number of $n$-tuples at the intersection of two patterns
intuitively relates to how similar these patterns are.  Nevertheless,
a better measure of similarity can be derived, using the disjunctive
box cluster model (\ref{eq:model}).  In particular, that better
measure can tell how close two non-overlapping patterns are.  First of
all, it must be observed that the inclusion of two patterns as
explanatory variables, in the disjunctive box cluster model of the
fuzzy tensor $T$, only influences the pattern-based reconstruction of
a sub-tensor $T'$ of $T$: the smallest sub-tensor where both patterns
are defined.  How much those two patterns explain $T'$ (a nested
disjunctive box cluster model) is a natural way to measure their
distance\footnote{The term \emph{distance} is here abused, \ie, the
  mathematical definition of a distance is not satisfied.  The
  so-called distance is in fact a similarity measure, where
  \emph{smaller} values mean ``more similar''.}.  If the membership
degrees in $T'$ are close to being uniformly distributed, modeling it
using two patterns, $X$ and $Y$, in it does not significantly improve
upon the model that simply predicts a constant membership degree, the
density of the sub-tensor.  In that case, $X$ and $Y$ are indeed close
to each other.  On the contrary, if $X$ and $Y$ are two ``clear''
patterns in $T'$ (with no or little intersection and a great contrast
between the membership degrees inside and outside the patterns), then
$X$ and $Y$ well explain the sub-tensor and are distant from each
other.  The next paragraph formalizes that distance.

Given two patterns $X = \prod_{i = 1}^n X_i$ and
$Y = \prod_{i = 1}^n Y_i$, let us denote
$X \sqcup Y = \prod_{i = 1}^n X_i \cup Y_i$ the $n$-tuples of the
related sub-tensor, $T'$ in the paragraph above.  That paragraph
argues that, in the framework of this article, a natural measure of
the distance between $X$ and $Y$ contrasts the residual sums of
squares of two disjunctive box cluster models of $T'$: the model that
only uses one explanatory variable, $X \sqcup Y$, \ie, that predicts
the membership degree $\lambda_0 + \lambda_{X \sqcup Y}$ (the density
of $X \sqcup Y$) for all $n$-tuples in $X \sqcup Y$, and the model
that additionally uses $X$ and $Y$ as explanatory variables.
Therefore, the proposed function to compute, in $O(|X \sqcup Y|)$
time, the distance between $X$ and $Y$ is
$(X, Y) \mapsto RSS_{T'}(\{X \sqcup Y\}) - RSS_{T'}(\{X \sqcup Y, X,
Y\})$.
In that expression, the tensor $T'$ can actually be substituted by
$T$, \ie,
$(X, Y) \mapsto RSS_T(\{X \sqcup Y\}) - RSS_T(\{X \sqcup Y, X, Y\})$
is the same function.  Indeed, both models predict $\lambda_0$ for any
$n$-tuple not in $X \sqcup Y$ and the difference of quadratic
residuals is null.  In fact, the predictions only differ for the
$n$-tuples in $X \cup Y$.

At that point, the attentive reader may have detected a flaw in the
proposal.  The problem deals with the above distance quantifying in an
\emph{absolute} way how much the two patterns explain the sub-tensor.
Since there is more to explain in a larger sub-tensor, larger patterns
tend to be more distant from each other, for the sole reason that they
are larger.  Explained more technically, the distance between the
patterns $X$ and $Y$ is a sum over the $n$-tuples in $X \cup Y$, its
terms are mainly positive (because $\lambda_X$ and $\lambda_Y$ are
better estimates of the membership degrees of the $n$-tuples in $X$
and $Y$ than $\lambda_{X \sqcup Y}$), and the more terms in that sum,
the greater the distance tends to be.  A normalization is needed, \ie,
the distance defined in the previous paragraph must be divided by a
distance of reference that takes into account the sizes, $|X_1|$,
\dots, $|X_n|$ and $|Y_1|$, \dots, $|Y_n|$, of the two patterns
$X = \prod_{i = 1}^n X_i$ and $Y = \prod_{i = 1}^n Y_i$.  A natural
choice is the distance that would be obtained if $X$ and $Y$ would
still cover the same $n$-tuples with the same membership degrees (the
distance should not depend on the intrinsic quality of each of the two
patterns), if $X_i \cap Y_i = \emptyset$ for all
$i \in \{1, \dots, n\}$, and, if, outside these two patterns, all
membership degrees would be $\lambda_0$.  In that scenario,
$\lambda_{X \sqcup Y} = \frac{|X|\lambda_X + |Y|\lambda_Y}{\prod_{i =
    1}^n |X_i| + |Y_i|}$
and, using that value, the distance of reference (the denominator of
the normalization) is
$|X|(\lambda_X - \lambda_{X \sqcup Y})^2 + |Y|(\lambda_Y - \lambda_{X
  \sqcup Y})^2$.
% \begin{align*}
%   & (\lambda_{X \sqcup Y} - \lambda_X)\big(|X|(2\lambda_0 + \lambda_{X
%     \sqcup Y} + \lambda_X) - 2\sum_{t \in X} T_t\big)\\
%   + & (\lambda_{X \sqcup Y} - \lambda_Y)\big(|Y|(2\lambda_0 +
%       \lambda_{X \sqcup Y} + \lambda_Y) - 2\sum_{t \in Y} T_t\big)
% \end{align*}

The normalization does not alter, in big O notation, the time required
to compute the distance between two patterns $X$ and $Y$, \ie,
$O(|X \sqcup Y|)$.  In the context of an interactive visualization,
the distance may be needed at several occasions.  It can be stored
after computed for the first time.  For the whole disjunctive box
cluster model, using (half) a $|\mathcal{X}| \times |\mathcal{X}|$
matrix for the cache, the memory requirement is $O(|\mathcal{X}|^2)$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
