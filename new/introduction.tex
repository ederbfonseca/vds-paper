Many large datasets of interest relate two or more \emph{entities}.
For example, a data scientist may want to study the influence of
Twitter users (first entity type) writing about Brazilian soccer teams
(second entity type) week after week (third entity type).  How many
retweets a user received whenever she mentioned a given team during a
given week may be used as a proxy of her influence, w.r.t. the team,
during the week.  That number can be turned into a degree of
influence, in $[0, 1]$, through a process known in the literature as
\emph{fuzzification}.  $0$ indicates a total lack of influence (no
retweet).  $1$ stands for an very strong influence (``many'' retweets;
the actual minimal number depends on the parameters of the
fuzzification).  Degrees between $0$ and $1$ allow a smooth transition
between the two extremes.  After fuzzyfying all the numbers of
retweets associated with all the combinations of a user, a team and a
week, the analyst faces a 3D table whose ``slices'' relate to users
(first dimension), teams (second dimension) and weeks (third
dimension).  Each of its cells contain the influence degree, in
$[0, 1]$, associated with the triple $($user$,$ team$,$ week$)$
indexing it.  In more technical terms, that dataset is a $3$-way fuzzy
tensor.

For interpretation purpose, directly visualizing a large $n$-way fuzzy
tensor (say, with millions of cells) is very hard, if not impossible.
A summary of it is needed.  A \emph{local pattern} condenses part of
the information in a dataset.  For example, in a 0/1 (aka Boolean)
matrix, an itemset is a subset of columns and its supporting set is
the maximal set of rows such that, at the intersection with the
columns of the itemset, only $1$s are found.  Listing these rows and
columns being shorter than listing their Cartesian product, the
pattern indeed summarizes a relevant sub-matrix of the entire 0/1
matrix.  Itemsets have been generalized to $n$-way fuzzy
tensors~\cite{multidupehack}: the patterns become sub-tensors that
cover values that are ``mostly close'' to 1 (see \cite{multidupehack}
for a precise definition).  Even more interestingly, the data mining
community tackles the problem of actually summarizing \emph{whole}
tensors, using patterns.  Rather than listing all the patterns that
individually satisfy a rigid definition (what usually entails the
discovery of many small overlapping patterns), the idea is to search,
with heuristics, a small number of large patterns that altogether
allow an approximate reconstruction of the dataset.

The most popular approach is to approximately factorize the
tensor~\cite{tensor-decompositions}, typically using a
CANDECOMP/PARAFAC (CP) decomposition.  That approach was adapted to
0/1 data~\cite{Miettinen11}.  The objective becomes the search of a
small number of sub-tensors such that, filling a null tensor (whose
dimensions are those of the dataset) with $1$s in all cells covered by
at least one pattern, provides a tensor that is as close as possible
to the dataset: the number of cells at $0$ in the dataset but at $1$
in its pattern-based reconstruction, or vice versa, is minimized.  In
2011, Mirkin and Kramarenko~\cite{Mirkin:2011} proposed the
\emph{disjunctive box cluster model}, which is applicable to fuzzy
tensors, although the authors focus on 0/1 data.  That model can be
seen as a generalization of the CP decomposition of 0/1 tensors.  The
objective is still the discovery of a small number of sub-tensors that
support the approximate reconstruction of the dataset.  However, this
time, a cell covered by a pattern is not necessarily at $1$.  It is
filled with the density of the densest pattern covering it, where the
density of a pattern is the arithmetic mean of all the values it
covers in the dataset.  Figures~\ref{fig:tensor} and
\ref{fig:reconstruction} illustrate that reconstruction.  The
disjunctive box cluster model is actually a regression model: the
patterns are explanatory variables and the fuzzy tensor is the target
variable.  The ordinary least-squares approach justifies the use of
pattern densities: with non-overlapping patterns, they are the optimal
values to minimize the Frobenius norm of the difference between the
dataset and its pattern-based reconstruction.  The use of the $\max$
operator, at the intersection of several patterns, is justified as
well: the model is a disjunction of patterns and the $\max$ operator
is a fuzzy OR.

\begin{figure*}
  \centering
  \footnotesize
  % \setlength{\tabcolsep}{2.5pt}
  \begin{tabular}{rccccccccccccc}
    & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}} & \quad
    \quad & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}}\\
    \texttt{ESPNagora} & \light 1.00 & \dark 0.85 & \light 0.80 &
    \light 0.70 & 0.10 & \dark 0.95 & & \light 0.70 & 0.20 & \darker
    1.00 & \darker 1.00 & 0.25 & \light 0.80\\
    \texttt{Estadao} & \light 0.80 & \dark 0.90 & \light 0.70 & \light
    0.80 & 0.20 & \dark 0.90 & & \light 0.80 & 0.05 & \darker 0.75 &
    \darker 0.60 & 0.00 & \light 0.70\\
    \texttt{FlaNews} & \light 0.70 & 0.15 & \light 1.00 & \light 0.65
    & 0.00 & \light 0.70 & & \light 0.60 & 0.00 & \darker 1.00 &
    \darker 1.00 & 0.00 & \light 0.65\\
    \texttt{N5Eoficial} & \light 0.05 & \dark 1.00 & \light 0.05 &
    \light 0.00 & 0.00 & \dark 0.80 & & \light 0.00 & 1.00 & \light
    0.55 & \light 0.50 & 0.05 & \light 0.40\\
    \texttt{RenataBFan} & \light 0.80 & 0.15 & \light 0.75 & \light
    0.75 & 0.95 & \light 0.90 & & \light 0.85 & 0.05 & \light 0.70 &
    \light 0.60 & 0.85 & \light 0.80\\
    \texttt{SporTV} & \light 0.80 & 0.55 & \light 0.70 & \light 0.60 &
    0.05 & \light 0.80 & & \light 0.80 & 0.20 & \darker 1.00 & \darker
    1.00 & 0.10 & \light 0.75\\
    & \multicolumn{6}{c}{\texttt{week 45}} & &
    \multicolumn{6}{c}{\texttt{week 46}}\\
    & & & & & & & & & & & & &
  \end{tabular}
  \begin{tabular}{ccc}
    \scriptsize
    weeks & teams & users\\
    \light \texttt{week 45}, \texttt{week 46} &\light
                                                \texttt{ESPNagora},
                                                \texttt{Estadao},
                                                \texttt{FlaNews},
                                                \texttt{N5Eoficial},
                                                \texttt{RenataBFan},
                                                \texttt{SporTV} &
                                                                  \light
                                                                  \texttt{Corinthians},
                                                                  \texttt{Flamengo},
                                                                  \texttt{Fluminense},
                                                                  \texttt{São
                                                                  Paulo}\\
    \dark \texttt{week 45} & \dark \texttt{ESPNagora},
                             \texttt{Estadao}, \texttt{N5Eoficial} &
                                                                     \dark
                                                                     \texttt{Cruzeiro},
                                                                     \texttt{São
                                                                     Paulo}\\
    \darker \texttt{week 46} & \darker \texttt{ESPNagora},
                               \texttt{Estadao}, \texttt{FlaNews},
                               \texttt{SporTV} & \darker
                                                 \texttt{Flamengo},
                                                 \texttt{Fluminense}
  \end{tabular}
  \caption{Three patterns in a $2 \times 6 \times 6$ fuzzy tensor (toy
    dataset).  The bottom table orders the patterns by increasing
    density.  Those densities are the arithmetic means of the values
    covered the patterns: \colorbox{black!20}{0.79},
    \colorbox{black!40}{0.90} and \colorbox{black!60}{0.92}.  The
    \colorbox{black!20}{first pattern}, in the bottom table, overlaps
    with each of the two other patterns.  It is even a super-pattern
    of the \colorbox{black!60}{third pattern}.\label{fig:tensor}}
\end{figure*}

\begin{figure*}
  \centering
  \footnotesize
  % \setlength{\tabcolsep}{2.5pt}
  \begin{tabular}{rccccccccccccc}
    & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}} & \quad
    \quad & \rot{\texttt{Corinthians}} & \rot{\texttt{Cruzeiro}} &
    \rot{\texttt{Flamengo}} & \rot{\texttt{Fluminense}} &
    \rot{\texttt{Internacional}} & \rot{\texttt{São Paulo}}\\
    \texttt{ESPNagora} & \light 0.79 & \dark 0.90 & \light 0.79 &
    \light 0.79 & 0.00 & \dark 0.90 & & \light 0.79 & 0.00 & \darker
    0.92 & \darker 0.92 & 0.00 & \light 0.79\\
    \texttt{Estadao} & \light 0.79 & \dark 0.90 & \light 0.79 & \light
    0.79 & 0.00 & \dark 0.90 & & \light 0.79 & 0.00 & \darker 0.92 &
    \darker 0.92 & 0.00 & \light 0.79\\
    \texttt{FlaNews} & \light 0.79 & 0.00 & \light 0.79 & \light 0.79
    & 0.00 & \light 0.79 & & \light 0.79 & 0.00 & \darker 0.92 &
    \darker 0.92 & 0.00 & \light 0.79\\
    \texttt{N5Eoficial} & \light 0.79 & \dark 0.90 & \light 0.79 &
    \light 0.79 & 0.00 & \dark 0.90 & & \light 0.79 & 0.00 & \light
    0.79 & \light 0.79 & 0.00 & \light 0.79\\
    \texttt{RenataBFan} & \light 0.79 & 0.00 & \light 0.79 & \light
    0.79 & 0.00 & \light 0.79 & & \light 0.79 & 0.00 & \light 0.79 &
    \light 0.79 & 0.00 & \light 0.79\\
    \texttt{SporTV} & \light 0.79 & 0.00 & \light 0.79 & \light 0.79 &
    0.00 & \light 0.79 & & \light 0.79 & 0.00 & \darker 0.92 & \darker
    0.92 & 0.00 & \light 0.79\\
    & \multicolumn{6}{c}{\texttt{week 45}} & &
    \multicolumn{6}{c}{\texttt{week 46}}\\
    & & & & & & & & & & & & &
  \end{tabular}
  \caption{Approximate reconstruction of the toy dataset from the
    three patterns in Figure~\ref{fig:tensor}.  In any cell, the value
    is the density of the densest pattern covering the cell or, if no
    pattern covers it, 0.\label{fig:reconstruction}}
\end{figure*}

The data mining community keeps designing algorithms to discover
pattern-based summaries of fuzzy tensors, with fewer patterns and/or
that support a more accurate reconstruction.  On the other hand,
little has been proposed to help the interpretation of such summaries.
A simple list, such as the bottom table in Figure~\ref{fig:tensor}, is
certainly not the best way to present the patterns to the analyst.  A
well-designed visualization of pattern-based summaries is needed.  It
is a challenge too.  The patterns are multidimensional, possibly large
and, for an accurate summary, there may be thousands of them.  Maybe
more importantly, the relevant aspects that are worth visualizing must
be first identified.
% Raquel
% Due to the complexity, multidimensionality and possibly high volume of
% data and computed patterns, their analysis is challenging. Well
% designed graphical representation of these data and concepts are an
% external artifact and important cognitive tool. Interaction techniques
% further improve the visual analytical process making possible to the
% analyst to raise and answer questions about presented patterns and the
% fuzzy tensor. Consequently, interactive visualization is key to
% understand the patterns, to gain knowledge and insights into the fuzzy
% tensor and to communicate discoveries. Through the proposed tool she
% can inspect and interact with $n$-way fuzzy tensors to understand and
% validate the interesting information and patterns discovered by data
% mining algorithms.
% /Raquel
This article proposes, for the first time, an interactive
visualization of patterns seen as explanatory variables of a
disjunctive box cluster model.  Indeed, both the visualized
information and the interaction possibilities are mostly justified by
the model itself.

That is why, after discussing in Section~\ref{sec:related} previous
publications dealing with the visualization of collections of
patterns, Section~\ref{sec:model} formally presents the disjunctive
box cluster model and derives relevant information associated with the
individual patterns (their densities and areas), with pairs of
patterns (their possible inclusion, the size of their intersection,
and a similarity measure) and with the whole set of patterns (a total
order, to get coarser models).
% The patterns can partially overlap (as the first two patterns in
% Figure~\ref{fig:tensor}) or even be nested (as the first and third
% patterns in Figure~\ref{fig:tensor}).
Section~\ref{} considers those aspects.
% as well as those emphasized in the previous section
It translates them into either visual objects and attributes or
analytical interaction techniques.  Section~\ref{} invites the reader
to test the visualization and details a case study, using the retweet
data mentioned in the first paragraph of this article.  Finally,
Section~\ref{} briefly concludes.

% \begin{itemize}
% \item the density and the area of every pattern are visualized, for
%   directly relating to its contribution to the model;
% \item patterns can in fact be ordered by decreasing contribution so
%   that coarser models (less accurate but faster to interpret), with
%   less patterns, can be visualized;
% \item since patterns can partially overlap (as the first two patterns
%   in Figure~\ref{fig:tensor}), showing such intersections help the
%   navigation from a pattern to another;
% \item since patterns can even be included into each other (as the
%   first and third patterns in Figure~\ref{fig:tensor}), a hierarchical
%   navigation, which presents less patterns at once, is appropriate;
% \item a similarity between patterns can be defined and the patterns
%   spread out on the screen accordingly;
% \item finally, a search tool allows to easily filter the patterns
%   based on the elements they contain, for instance to only visualize
%   patterns involving a specific team, in the toy example.
% \end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
